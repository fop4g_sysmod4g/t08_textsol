#include <assert.h>

#include "SFML/Graphics.hpp"

#include "GameConsts.h"

using namespace std;

/*
Big hacky demo of all the ways you can use text rendering in SFML
*/

int main()
{
	// Create the main window
	sf::RenderWindow window(sf::VideoMode(GC::SCREEN_RES.x, GC::SCREEN_RES.y), "My first code");

	// Load a sprite to display
	sf::Texture texture;
	if (!texture.loadFromFile("data/fancy_buttons.png"))
		assert(false);

	sf::Font font;
	if (!font.loadFromFile("data/fonts/comic.ttf"))
		assert(false);

	const float SCALE_INC = 0.001f;
	float scale = 0;
	const float ROT_INC = 0.001f;
	float rot = 0;
	const float TRANS_INC = 0.001f, TRANS_RAD = 100.f;
	float trans = 0;
	string name;
	bool nameComplete = false;
	// Start the game loop 
	while (window.isOpen())
	{
		// Process events
		sf::Event event;
		while (window.pollEvent(event))
		{
			// Close window: exit
			if (event.type == sf::Event::Closed) 
				window.close();
			if (event.type == sf::Event::TextEntered)
			{
				if (event.text.unicode == GC::ESCAPE_KEY)
					window.close();
				if (event.text.unicode == 13 && !name.empty())
					nameComplete = true;
				if (isdigit(event.text.unicode) || isalpha(event.text.unicode))
					name += static_cast<char>(event.text.unicode);
			}
		} 

		// Clear screen
		window.clear();

		//instantiate and draw a button with some text lined up on top
		sf::Sprite spr(texture);
		sf::IntRect rect{7,8,96,31};
		spr.setTextureRect(rect);
		spr.setOrigin(rect.width / 2.f, rect.height / 2.f);
		spr.setPosition(window.getSize().x / 2.f, window.getSize().y / 2.f);
		window.draw(spr);

		sf::Text txt3("PLAY", font, 14);
		sf::FloatRect fr = txt3.getLocalBounds();
		txt3.setOrigin(fr.width / 2.f, fr.height / 2.f);
		txt3.setPosition(window.getSize().x / 2.f, window.getSize().y / 2.f);
		window.draw(txt3);

		//ask the use what their name is
		string msg;
		if (nameComplete)
			msg = "Welcome ";
		else
			msg = "Enter your name (press return): ";
		msg += name;
		txt3.setString(msg);
		fr = txt3.getLocalBounds();
		txt3.setOrigin(fr.width / 2.f, fr.height / 2.f);
		txt3.setPosition(txt3.getPosition().x, txt3.getPosition().y + fr.height*2.f);
		window.draw(txt3);

		//left justified super simple text 
		sf::Text txt("Fezz was here", font, 30);
		txt.setPosition(10,10);
		window.draw(txt);

		
		//scaling some text in and out
		txt.setString("That's no moon!!");
		fr = txt.getGlobalBounds();
 		float alpha = sinf(scale) + 2.f;
		txt.setCharacterSize((int)(txt.getCharacterSize()*alpha));
		fr = txt.getGlobalBounds();
		scale += SCALE_INC;
		float x = window.getSize().x - fr.width - fr.left, y = fr.height / 2.f - fr.top;
		txt.setPosition(x,y);
		txt.setFillColor(sf::Color::Red);
		window.draw(txt);

		//rotating using a sine wave looks more interesting than a simple rotate
		sf::Text txt2("Fear is the mind killer", font, 30);
		fr = txt2.getLocalBounds();
		txt2.setOrigin(fr.width / 2.f, fr.height / 2.f);
		txt2.setPosition(window.getSize().x*0.85f, window.getSize().y*0.8f);
		txt2.setRotation(sinf(rot)*360);
		rot += ROT_INC;
		txt2.setFillColor(sf::Color::Yellow);
		window.draw(txt2);

		//translating around the circumference of a circle
		txt2.setString("They come at night, mostly");
		sf::Vector2f pos(window.getSize().x*0.25f, window.getSize().y*0.8f);
		pos.x += sinf(trans)*TRANS_RAD;
		pos.y += cosf(trans)*TRANS_RAD;
		trans += TRANS_INC;
		txt2.setPosition(pos);
		txt2.setFillColor(sf::Color::Green);
		txt2.setRotation(0);
		window.draw(txt2);



		// Update the window
		window.display();
	}

	return EXIT_SUCCESS;
}
